import base64
import json

from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.hazmat.primitives.serialization import load_pem_private_key

key_file = open("domain.key", "rb")
input_file = open("input.json", "r")

# get rid of the pretty printing to produce compact JSON
msg = json.dumps(json.loads(input_file.read()), separators=(',', ':'))
print(msg)
print()  # adds empty line to make output clearer

private_key = load_pem_private_key(key_file.read(), '1234'.encode('utf-8'))
signature = private_key.sign(msg.encode('utf-8'), padding.PKCS1v15(), hashes.SHA256())

print(base64.b64encode(signature).decode('utf-8'))
