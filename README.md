# RSA Signature

A signature is a bit of digital information that can be verified to have originated from a specific source. A plaint text message that anyone can read usually accompanies it.

## What the example covers

1. Create a key pair of certificate and private key using OpenSSL
2. Create a compact JSON string from the example json file provided in `input.json`
3. Create a signature of that string using the private key
4. Optionally confirm that it is possible to verify that signature with just the public key

### What you will need:

* Python (tested with 3.11)
* Poetry - you can skip this and use pip if you prefer that
* OpenSSL

### Generate the keys

    openssl req -newkey rsa:2048 -keyout domain.key -x509 -days 365 -out domain.crt

**Hint: set the passphrase to 1234**

### Install dependencies

There's one simple dependency in this project (pycryptodome). Adapt this to whatever package manager you prefer to use

    poetry install

### Run the script

    python signature.py

The output should look something like this (second line will vary):

    {"message":"Hello World","firstName":"John","lastName":"Doe"}

    KX2rFQZk4Rz0V60yN+RgpL8Ch8PdWf0mjfWTe7lIrsOrCUF/zTOhRNi6LFnWAzUB1b9JbtXQuROXJ6PcB4JU5nmzmhRiv6EKCYn/22Ry7TmI1uWQ/LzRHGaNkFnVxIr6e2LxEyf8mn9r0TO4ynZoCmMlnmHinUlGs1vVEffULBPUw9PZClPuYIjzGU+rqpOAhWO6w64wR72s03QrNIniZZit63WEi8kU47L+vqC0urFlRIsUrHAUEAXWoKaTAKmsncXOm9/r3fLMucf7wX8Gm9Mw7d2cPMBsWCrJQex0xclPYTAxDWD1DPORGCxpmy4Ts7DgneprP3tSOxrBhN4rug==

Where the first line is the compact JSON you will generate the signature from and the second line is the signature

## Verification

To visually confirm this is a valid signature, visit https://8gwifi.org/RSAFunctionality?rsasignverifyfunctions=rsasignverifyfunctions&keysize=2048

_*PS*: Never put your private key out on the internet, regardless of what claims the site may make. Only use your public key here._

Before you use the public key for verification on this site, you will need to convert it

    openssl rsa -in domain.key -RSAPublicKey_out -out pubkeyrsa.pem

Copy the JSON, signature and `pubkeyrsa.pem` content to the appropriate input fields and select the verification option
with SHA256withRSA
